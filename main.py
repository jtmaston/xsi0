import click


def check_line(game_list):
    test = game_list[0]
    flag = True
    counter = 0
    for i in range(0, 3):
        if game_list[i] != test or game_list[i] == ' ':
            flag = False
        else:
            counter += 1
    if flag and counter == 3:
        return True
    else:
        counter = 0
    flag = True
    test = game_list[3]
    for i in range(3, 6):
        if game_list[i] != test or game_list[i] == ' ':
            flag = False
        else:
            counter += 1
    if flag and counter == 3:
        return True
    else:
        counter = 0
    flag = True
    test = game_list[6]
    for i in range(6, 9):
        if game_list[i] != test or game_list[i] == ' ':
            flag = False
        else:
            counter += 1
    if flag and counter == 3:
        return True
    return False


def check_column(game_list):
    test = game_list[0]
    flag = True
    counter = 0
    for i in range(0, 7, 3):
        if game_list[i] != test or game_list[i] == ' ':
            flag = False
        else:
            counter += 1
    if flag and counter == 3:
        return True
    else:
        counter = 0
    flag = True
    test = game_list[3]
    for i in range(1, 8, 3):
        if game_list[i] != test or game_list[i] == ' ':
            flag = False
        else:
            counter += 1
    if flag and counter == 3:
        return True
    else:
        counter = 0
    flag = True
    test = game_list[6]
    for i in range(2, 9, 3):
        if game_list[i] != test or game_list[i] == ' ':
            flag = False
        else:
            counter += 1
    if flag and counter == 3:
        return True
    return False


def check_primary(game_list):
    test = game_list[0]
    for i in range(0, 10, 4):
        if game_list[i] == ' ':
            return False
        if game_list[i] != test:
            return False
    return True


def check_secondary(game_list):
    test = game_list[2]
    for i in range(2, 7, 2):
        if game_list[i] == ' ':
            return False
        if game_list[i] != test:
            return False
    return True


def still_playing(game_list):
    if check_line(game_list):
        return False
    if check_column(game_list):
        return False
    if check_secondary(game_list):
        return False
    if check_primary(game_list):
        return False
    return True


def get_coords():
    while True:
        try:
            x, y = int(input("X: ")), int(input("Y: "))
            if not (0 < x < 4 and 0 < y < 4):
                raise OverflowError
            else:
                break
        except ValueError:
            print("Incercati din nou!")
        except OverflowError:
            print("Introduceti numai numere intre 1 si 3")
    if y == 1:
        return x - 1
    elif y == 2:
        return x + 2
    elif y == 3:
        return x + 5


def unscramble(a):
    if str(a) == 'True':
        return 'O'
    elif str(a) == 'False':
        return 'X'
    elif a == ' ':
        return ' '
    else:
        return "ERRVAL"


def draw_board(game_list):
    print('-' * 7)
    print('|' + unscramble(game_list[0]) + '|' + unscramble(game_list[1]) + '|' + unscramble(game_list[2]) + '|')
    print('|' + unscramble(game_list[3]) + '|' + unscramble(game_list[4]) + '|' + unscramble(game_list[5]) + '|')
    print('|' + unscramble(game_list[6]) + '|' + unscramble(game_list[7]) + '|' + unscramble(game_list[8]) + '|')
    print('-' * 7)


def main():
    board = [' '] * 9
    active_player = False
    still_here = True
    print("Welcome to our game! Player one starts.")
    while still_here:
        click.clear()
        draw_board(board)
        print(active_player + 1)
        t = get_coords()
        if board[t] == ' ':
            board[t] = active_player
            active_player = not active_player
        else:
            print("Hey! That spot's already taken!")
            continue
        still_here = still_playing(board)
    active_player = not active_player
    draw_board(board)
    print("Great job, Player " + str(int(active_player) + 1) + ", you win!")


main()
